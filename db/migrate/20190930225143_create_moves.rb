class CreateMoves < ActiveRecord::Migration[6.0]
  def change
    create_table :moves do |t|
    	t.integer :x_start
    	t.integer :y_start
    	t.integer :x_destination
    	t.integer :y_destination
    	t.integer :game_id
    	t.integer :piece_id
    	t.string :piece_type
    end
  end
end
