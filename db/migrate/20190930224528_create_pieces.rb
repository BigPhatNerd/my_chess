class CreatePieces < ActiveRecord::Migration[6.0]
  def change
    create_table :pieces do |t|
    	t.string :name
    	t.boolean :white
    	t.integer :game_id
    	t.integer :x_coord
    	t.integer :piece_type
    	t.string :html_code
    	
    end
  end
end
